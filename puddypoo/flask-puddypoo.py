#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pudppo


# In[11]:


#pudppo.poojob('william', 'just wee')


# In[2]:


#pudppo.howlongago()


# In[2]:


#pudppo.getallpoocleans()


# In[3]:


#pudppo.getpoocleans()


# In[ ]:


from flask import Flask, request
from flask_restful import reqparse, abort, Api, Resource
app = Flask(__name__)
api = Api(app)

#recentclean = pudppo.getpoocleans()
#recentclean

#def abort_if_todo_doesnt_exist(todo_id):
#    if todo_id not in TODOS:
#        abort(404, message="Todo {} doesn't exist".format(todo_id))

#parser = reqparse.RequestParser()
#parser.add_argument('task')


# Todo
# shows a single pooclean item and lets you delete a pooclean item
class RecentClean(Resource):
    def get(self):
        #abort_if_todo_doesnt_exist(todo_id)
        return(pudppo.getpoocleans())
        


    #def delete(self, todo_id):
        #abort_if_todo_doesnt_exist(todo_id)
        #del TODOS[todo_id]
        #return '', 204

    def put(self, user, comment):
        
        #args = parser.parse_args()
        #task = {'task': args['task']}
        #TODOS[todo_id] = task
        return pudppo.poojob(user, comment), 201


# Poojob List
# shows a list of all poojobs, and lets you POST to add new tasks
class PooJobsList(Resource):
    def get(self):
        return pudppo.getallpoocleans()
    
    #def post(self, user, comment):
    #    return pudppo.poojob(user, comment), 201
        #args = parser.parse_args()
    #    todo_id = int(max(TODOS.keys()).lstrip('todo')) + 1
    ##    todo_id = 'todo%i' % todo_id
    #    TODOS[todo_id] = {'task': args['task']}
    #    return TODOS[todo_id], 201
    
    def post(self):
        json_data = request.get_json(force=True)
        user = json_data['user']
        comment = json_data['comment']
        #args = parser.parse_args()
        #un = str(args['username'])
        #pw = str(args['password'])
        return pudppo.poojob(user, comment), 201

##
## Actually setup the Api resource routing here
##
api.add_resource(PooJobsList, '/poojobs')
api.add_resource(RecentClean, '/recentclean')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port='5556')

